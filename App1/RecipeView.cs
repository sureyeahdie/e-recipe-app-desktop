﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    public class RecipeView
    {
        public int id { get; private set; }
        public int recipe_no { get; private set; }
        public string doses { get; private set; }
        public string signature { get; private set; }
        public int quantity { get; private set; }
        public int medicine_id { get; private set; }
        public int doctor_id { get; private set; }
        public int patient_id { get; private set; }
        public int clinic_id { get; private set; }

        public string patientName { get; private set; }
        public string doctorName { get; private set; }
        public string medicine { get; private set; }
        public int price { get; private set; }
        public int total_price { get; private set; }
        public DateTime recipeDate { get; private set; }


        public RecipeView(int id, int recipe_no, string doses, string signature,
            int quantity, int medicine_id, int doctor_id, int patient_id,
            int clinic_id, string doctorName, string patientName, string medicine, int price,
            int year, int month, int day)
        {
            this.id = id;
            this.recipe_no = recipe_no;
            this.doses = doses;
            this.signature = signature;
            this.quantity = quantity;
            this.medicine_id = medicine_id;
            this.doctor_id = doctor_id;
            this.patient_id = patient_id;
            this.clinic_id = clinic_id;
            this.patientName = patientName;
            this.doctorName = doctorName;
            this.medicine = medicine;
            this.price = price;
            this.recipeDate = new DateTime(year, month, day);
            total_price = this.price * this.quantity;
        }
    }
}
