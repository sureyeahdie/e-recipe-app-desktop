﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    public class Connect
    {
        private string base_url = "http://localhost:8080/e_recipe_API/e-recipe-api/public/v1/";
        public async Task<string> req_adapter(string target)
        {
            string url = this.base_url + target;
            try
            {
                HttpClient client = new HttpClient();
                HttpResponseMessage response = await client.GetAsync(url);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                response.Dispose();
                return responseBody;
            }
            catch
            {
                return "{\"success\" : false}";
            }
            
        }
    }
}
