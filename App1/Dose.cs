﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    public class Dose
    {
        public int id { get; private set; }
        public int min_dose { get; private set; }
        public int max_dose { get; private set; }
        public string unit { get; private set; }
        public int min_age { get; private set; }
        public int max_age { get; private set; }
        public int time_1 { get; private set; }
        public string time_unit_1 { get; private set; }
        public int time_2 { get; private set; }
        public string time_unit_2 { get; private set; }
        public int category_id { get; private set; }
        public string doseString { get; private set; }


        public Dose(int id, int min_dose, int max_dose, string unit,
            int min_age, int max_age,int time_1, string time_unit_1,
            int time_2, string time_unit_2, int category_id)
        {
            this.id = id;
            this.min_dose = min_dose;
            this.max_dose = max_dose;
            this.unit = unit;
            this.min_age = min_age;
            this.max_age = max_age;
            this.time_1 = time_1;
            this.time_unit_1 = time_unit_1;
            this.time_2 = time_2;
            this.time_unit_2 = time_unit_2;
            this.category_id = category_id;
            doseString = min_dose + " " + unit + " - " +
            max_dose + " " + unit + ", setiap " + time_1 + " " + time_unit_1 +
            " atau " + time_2 + " " + time_unit_2;
        }
    }
}
