﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    public class Recipe
    {
        public int id { get; private set; }
        public int recipe_no { get; private set; }
        public string doses { get; private set; }
        public string signature { get; private set; }
        public int quantity { get; private set; }
        public int medicine_id { get; private set; }
        public int doctor_id { get; private set; }
        public int patient_id { get; private set; }
        public int clinic_id { get; private set; }

        public string patientName { get; private set; }
        public string doctorName { get; private set; }
        public string medicine { get; private set; }

        private ObservableCollection<Patient> patients = new ObservableCollection<Patient>();
        private ObservableCollection<Doctor> doctors = new ObservableCollection<Doctor>();
        private Patient patient;
        private Doctor doctor;
        public Recipe(int id, int recipe_no, string doses, string signature,
            int quantity, int medicine_id, int doctor_id, int patient_id,
            int clinic_id)
        {
            this.id = id;
            this.recipe_no = recipe_no;
            this.doses = doses;
            this.signature = signature;
            this.quantity = quantity;
            this.medicine_id = medicine_id;
            this.doctor_id = doctor_id;
            this.patient_id = patient_id;
            this.clinic_id = clinic_id;
        }

        private Patient getPatient(int pid)
        {
            Patient patient;
            patients = PatientAL.getPatients();
            return patient = patients.Single(x => x.id == pid);
        }

        private Doctor getDoctor(int pid)
        {
            Doctor doctor;
            doctors = DoctorAL.getDoctors();
            return doctor = doctors.Single(x => x.id == pid);
        }
    }
}
