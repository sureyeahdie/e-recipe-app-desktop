﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    public class PharmacistsAL
    {
        private static ObservableCollection<Pharmacist> pharmacists = new ObservableCollection<Pharmacist>();
        private static ObservableCollection<string> pharmacistsName = new ObservableCollection<string>();
        private static string data;


        private static async void setPharmacistsList()
        {
            Connect conn = new Connect();
            pharmacists = new ObservableCollection<Pharmacist>();
            string result = await conn.req_adapter("pharmacists/view-all");

            dynamic obj = JsonConvert.DeserializeObject(result);

            if (obj.success == "true")
            {
                foreach (var d in obj.data)
                {
                    string str_id = d.id;
                    string name = d.name;
                    string email = d.email;
                    string phone_number = d.phone_number;
                    string SIK = d.SIK;

                    int id = Int32.Parse(str_id);
                    Pharmacist doc = new Pharmacist(id, name, email, phone_number, SIK);

                    pharmacists.Add(doc);
                }
                setPharmacistsName();


            }
            else
            {
                data = "no data";
                var msg = new Windows.UI.Popups.MessageDialog(
                    "No Data Found.");
            }

        }

        private static void setPharmacistsName()
        {
            foreach (Pharmacist d in pharmacists)
            {
                data = d.name;
                pharmacistsName.Add(d.name.ToString());
            }

        }

        public static ObservableCollection<Pharmacist> getPharmacists()
        {
            setPharmacistsList();
            return pharmacists;
        }

        public static ObservableCollection<string> getPharmacistsName()
        {

            return pharmacistsName;
        }

        public static string debugDoctors()
        {
            return data;
        }
    }
}
