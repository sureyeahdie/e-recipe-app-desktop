﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    public class Doctor
    {
        public int id { get;  private set; }
        public string name { get; private set; }
        public string email { get; private set; }
        public string phone_number { get; private set; }
        public string specialist { get; private set; }

        public Doctor(int id, string name, string email, string phone_number, string specialist)
        {
            this.id = id;
            this.name = name;
            this.email = email;
            this.phone_number = phone_number;
            this.specialist = specialist;
        }
    }
}
