﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    public class PatientAL
    {
        private static ObservableCollection<Patient> patients = new ObservableCollection<Patient>();
        private static ObservableCollection<string> patientsName = new ObservableCollection<string>();
        private static string data;
        private static async void setPatientsList()
        {
            Connect conn = new Connect();
            patients = new ObservableCollection<Patient>();
            string result = await conn.req_adapter("patients/view-all");

            dynamic obj = JsonConvert.DeserializeObject(result);

            if (obj.success == "true")
            {
                foreach (var p in obj.data)
                {
                    string str_id = p.id;
                    string name = p.name;
                    string email = p.email;
                    string phone_number = p.phone_number;
                    string address = p.address;
                    string str_birth_date = p.birth_date;
                    string gender = p.gender;
                    data = str_birth_date;
                    int id = Int32.Parse(str_id);
                    string[] arr = new string[3];
                    arr[0] = "";arr[1] = "";arr[2] = "";
                    int i = 0;
                    foreach(char x in str_birth_date)
                    {
                        if(x == '-')
                        {
                            i++;
                        }
                        else
                        {
                            arr[i] += x;
                        }
                    }
                    data = Int32.Parse(arr[1]).ToString();
                    //DateTime birth_date = new DateTime(Int32.Parse(arr[0]), Int32.Parse(arr[1]),Int32.Parse(arr[2]));
                    DateTime birth_date = new DateTime(1980, 1, 3);
                    Patient patient = new Patient(id, name, email, phone_number, 
                        address, Int32.Parse(arr[0]), Int32.Parse(arr[1]), Int32.Parse(arr[2]), gender);

                    patients.Add(patient);
                }
                setPatientsName();


            }
            else
            {
                data = "no data";
                var msg = new Windows.UI.Popups.MessageDialog(
                    "No Data Found.");
            }

        }

        private static void setPatientsName()
        {
            foreach (Patient d in patients)
            {

                patientsName.Add(d.name.ToString());
            }

        }

        public static ObservableCollection<Patient> getPatients()
        {
            setPatientsList();
            return patients;
        }

        public static ObservableCollection<string> getPatientsName()
        {

            return patientsName;
        }


        public static string debugPatiens()
        {
            return data;
        }
    }
}
