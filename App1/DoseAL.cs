﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    public class DoseAL
    {
        private static ObservableCollection<Dose> doses = new ObservableCollection<Dose>();
        private static ObservableCollection<string> dosesList = new ObservableCollection<string>();
        private static string data;


        private static async void setCategoriesList()
        {
            Connect conn = new Connect();
            doses = new ObservableCollection<Dose>();
            string result = await conn.req_adapter("doses/view-all");
            data = result;
            dynamic obj = JsonConvert.DeserializeObject(result);

            if (obj.success == "true")
            {
                foreach (var d in obj.data)
                {
                    string str_id = d.id;
                    string str_min_dose = d.min_dose;
                    string str_max_dose = d.max_dose;
                    string unit = d.unit;
                    string str_min_age = d.min_age;
                    string str_max_age = d.max_age;
                    string str_time_1 = d.time_1;
                    string time_unit_1 = d.time_unit_1;
                    string str_time_2 = d.time_2;
                    string time_unit_2 = d.time_unit_2;
                    string str_category_id = d.category_id;

                    int id = Int32.Parse(str_id);
                    int min_dose = Int32.Parse(str_min_dose);
                    int max_dose = Int32.Parse(str_max_dose);
                    int min_age = Int32.Parse(str_min_age);
                    int max_age = Int32.Parse(str_max_age);
                    int time_1 = Int32.Parse(str_time_1);
                    int time_2 = Int32.Parse(str_time_2);
                    int category_id = Int32.Parse(str_category_id);


                    Dose doc = new Dose(id, min_dose, max_dose, unit, 
                        min_age, max_age, time_1, time_unit_1,
                        time_2, time_unit_2, category_id);

                    doses.Add(doc);
                    
                }
                setDosesStringList();

            }
            else
            {
                data = "no data";
                var msg = new Windows.UI.Popups.MessageDialog(
                    "No Data Found.");
            }

        }

        private static void setDosesStringList()
        {
            foreach (Dose d in doses)
            {
                string min_dose = d.min_dose.ToString();
                string max_dose = d.max_dose.ToString();
                string unit = d.unit;
                string time_1 = d.time_1.ToString();
                string time_unit_1 = d.time_unit_1;
                string time_2 = d.time_2.ToString();
                string time_unit_2 = d.time_unit_2;
                string dose_string = min_dose + " " + unit + " - " +
                    max_dose + " " + unit + ", setiap " + time_1 + " " + time_unit_1 +
                    " atau " + time_2 + " " + time_unit_2;
                data = dose_string;
                dosesList.Add(dose_string);

            }
        }

        public static ObservableCollection<Dose> getDoses()
        {
            setCategoriesList();
            return doses;
        }

        public static ObservableCollection<string> getDosesList()
        {
            return dosesList;
        }

        public static string debugDoses()
        {
            return data;
        }
    }
}

