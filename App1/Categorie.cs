﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    public class Categorie
    {
        public int id { get; private set; }
        public string name { get; private set; }
        public string usage { get; private set; }
        public string prohibition { get; private set; }
        public string info { get; private set; }

        public Categorie(int id, string name, string usage, string prohibition, string info)
        {
            this.id = id;
            this.name = name;
            this.usage = usage;
            this.prohibition = prohibition;
            this.info = info;
        }

    }
}
