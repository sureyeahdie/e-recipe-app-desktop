﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    public class CategorieAL
    {
        private static ObservableCollection<Categorie> categories = new ObservableCollection<Categorie>();
        private static ObservableCollection<string> categoriesName = new ObservableCollection<string>();
        private static string data;

      
        private static async void setCategoriesList()
        {
            Connect conn = new Connect();
            categories = new ObservableCollection<Categorie>();
            string result = await conn.req_adapter("categories/view-all");

            dynamic obj = JsonConvert.DeserializeObject(result);

            if (obj.success == "true")
            {
                foreach (var d in obj.data)
                {
                    string str_id = d.id;
                    string name = d.name;
                    string usage = d.usage;
                    string prohibition = d.prohibition;
                    string info = d.info;

                    int id = Int32.Parse(str_id);
                    Categorie doc = new Categorie(id, name, usage, prohibition, info);               

                    categories.Add(doc);
                }
                setCategoriesName();


            }
            else
            {
                data = "no data";
                var msg = new Windows.UI.Popups.MessageDialog(
                    "No Data Found.");
            }

        }

        private static void setCategoriesName()
        {
            foreach (Categorie d in categories)
            {
                data = d.name;
                categoriesName.Add(d.name.ToString());
            }

        }

        public static ObservableCollection<Categorie> getCategories()
        {
            setCategoriesList();
            return categories;
        }

        public static ObservableCollection<string> getCategoriesName()
        {

            return categoriesName;
        }

        public static string debugDoctors()
        {
            return data;
        }
    }
}
