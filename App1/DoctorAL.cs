﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    public class DoctorAL: INotifyPropertyChanged
    {
        private static ObservableCollection<Doctor> doctors = new ObservableCollection<Doctor>();
        private static ObservableCollection<string> doctorName = new ObservableCollection<string>();
        private static string data;

        public event PropertyChangedEventHandler PropertyChanged;

        private static async void setDoctorsList()
        {
            Connect conn = new Connect();
            doctors = new ObservableCollection<Doctor>();
            string result = await conn.req_adapter("doctors/view-all");
           
            dynamic obj = JsonConvert.DeserializeObject(result);
  
            if (obj.success == "true")
            {
                foreach (var d in obj.data)
                {
                    string str_id = d.id;
                    string name = d.name;
                    string email = d.email;
                    string phone_number = d.phone_number;
                    string specialist = d.specialist;

                    int id = Int32.Parse(str_id);
                    Doctor doc = new Doctor(id, name, email, phone_number, specialist);

                    doctors.Add(doc);
                }
                setDoctorName();

                    
            }
            else
            {
                data = "no data";
                var msg = new Windows.UI.Popups.MessageDialog(
                    "No Data Found.");
            }

        }

        private static void setDoctorName()
        {
            foreach(Doctor d in doctors)
            {
                data = d.name;
                doctorName.Add(d.name.ToString());
            }

        }

        public static ObservableCollection<Doctor> getDoctors()
        {
            setDoctorsList();
            return doctors;
        }

        public static ObservableCollection<string> getDoctorsName()
        {
            
            return doctorName;
        }

        public static string debugDoctors()
        {
            return data;
        }
    }
}
