﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    public class RecipeAL
    {
        private static ObservableCollection<Recipe> recipes = new ObservableCollection<Recipe>();
        private static ObservableCollection<Doctor> doctors = new ObservableCollection<Doctor>();
        private static ObservableCollection<Patient> patients = new ObservableCollection<Patient>();
        private ObservableCollection<Medicine> medicines = new ObservableCollection<Medicine>();
        private static ObservableCollection<RecipeView> recipesView = new ObservableCollection<RecipeView>();
        private static ObservableCollection<string> dosesList = new ObservableCollection<string>();
        private static string data;
        private static string responJsonText;

        private static async void setRecipesList(ObservableCollection<Doctor> doctors, ObservableCollection<Patient> patients, 
            ObservableCollection<Medicine> medicines)
        {
            Connect conn = new Connect();
            recipes = new ObservableCollection<Recipe>();
            recipesView = new ObservableCollection<RecipeView>();
            string result = await conn.req_adapter("recipes/view-all");
            dynamic obj = JsonConvert.DeserializeObject(result);

            if (obj.success == "true")
            {
                foreach (var d in obj.data)
                {
                    string str_id = d.id;
                    string str_recipe_no = d.recipe_no;
                    string doses = d.doses;
                    string signature = d.signature;
                    string str_quantity = d.quantity;
                    string str_medicine_id = d.medicine_id;
                    string str_doctor_id = d.doctor_id;
                    string str_patient_id = d.patient_id;
                    string str_clinic_id = d.clinic_id;
                    string str_date = d.created_at;

                    int id = Int32.Parse(str_id);
                    int recipe_no = Int32.Parse(str_recipe_no);
                    int quantity = Int32.Parse(str_quantity);
                    int medicine_id = Int32.Parse(str_medicine_id);
                    int doctor_id = Int32.Parse(str_doctor_id);
                    int patient_id = Int32.Parse(str_patient_id);
                    int clinic_id = Int32.Parse(str_clinic_id);
             
                    int doctorID = doctor_id;
                    Doctor doctorOBJ = doctors.Single(x => x.id == doctorID);
                    string doctorName = doctorOBJ.name;

                    int patientID = patient_id;
                    Patient patientOBJ = patients.Single(x => x.id == patientID);
                    string patientName = patientOBJ.name;

                    int medicineID = medicine_id;
                    Medicine medicineOBJ = medicines.Single(x => x.id == medicineID);
                    string medicineName = medicineOBJ.name;
                    int price = medicineOBJ.price;

                    string[] arr = new string[3];
                    arr[0] = ""; arr[1] = ""; arr[2] = "";
                    int i = 0;
                    foreach (char x in str_date)
                    {
                        if (x == '-')
                        {
                            i++;
                        }
                        else
                        {
                            arr[i] += x;
                        }
                        if(x == ' ')
                        {
                            break;
                        }
                    }


                    Recipe doc = new Recipe(id, recipe_no, doses, signature,
                        quantity, medicine_id, doctor_id, patient_id,
                        clinic_id);
                          
                    RecipeView docView = new RecipeView(id, recipe_no, doses, signature,
                    quantity, medicine_id, doctor_id, patient_id,
                    clinic_id, doctorName, patientName, medicineName, price,
                    Int32.Parse(arr[0]), Int32.Parse(arr[1]), Int32.Parse(arr[2]));

                    recipes.Add(doc);
                    recipesView.Add(docView);
                }
                

            }
            else
            {
                data = "no data";
                var msg = new Windows.UI.Popups.MessageDialog(
                    "No Data Found.");
            }

        }


        public async static void createRecipe(Recipe recipe)
        {
            Uri requestUri = new Uri("http://localhost:8080/e_recipe_API/e-recipe-api/public/v1/recipes/create");
            dynamic dynamicJson = new ExpandoObject();
            dynamicJson.recipe_no = recipe.recipe_no;
            dynamicJson.doses = recipe.doses;
            dynamicJson.signature = recipe.signature;
            dynamicJson.quantity = recipe.quantity;
            dynamicJson.medicine_id = recipe.medicine_id;
            dynamicJson.doctor_id = recipe.doctor_id;
            dynamicJson.patient_id = recipe.patient_id;
            dynamicJson.clinic_id = recipe.clinic_id;
            string json = "";
            json = JsonConvert.SerializeObject(dynamicJson);
            var objClient = new HttpClient();
            HttpResponseMessage respond = await objClient.PostAsync(requestUri,
                new StringContent(json, Encoding.UTF8, "application/json"));
            responJsonText = await respond.Content.ReadAsStringAsync();
        }

        public static ObservableCollection<RecipeView> getRecipesView(ObservableCollection<Doctor> doctors, ObservableCollection<Patient> patients,
            ObservableCollection<Medicine> medicines)
        {
            setRecipesList(doctors,patients,medicines);
            //return recipes;
            return recipesView;
        }

 /*       public static ObservableCollection<Recipe> getRecipes(ObservableCollection<Patient> patient)
        {
            setRecipesList(patients);
            return recipes;
        }*/

        public static string debugRecipes()
        {
            return data;
        }
    }
}

