﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    public class Pharmacist
    {
        public int id { get; private set; }
        public string name { get; private set; }
        public string email { get; private set; }
        public string phone_number { get; private set; }
        public string SIK { get; private set; }

        public int age { get; private set; }

        public Pharmacist(int id, string name, string email, string phone_number,
            string SIK)
        {
            this.id = id;
            this.name = name;
            this.email = email;
            this.phone_number = phone_number;
            this.SIK = SIK;

        }
    }
}
