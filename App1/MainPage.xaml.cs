﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace App1
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
      
        private ObservableCollection<Medicine> medicinesList = new ObservableCollection<Medicine>();
        private ObservableCollection<string> medicinesName = new ObservableCollection<string>();
        private ObservableCollection<Doctor> doctorsList = new ObservableCollection<Doctor>();
        private ObservableCollection<string> doctorName = new ObservableCollection<string>();
        private ObservableCollection<Patient> patients = new ObservableCollection<Patient>();
        private ObservableCollection<string> patientsName = new ObservableCollection<string>();
        private ObservableCollection<Pharmacist> pharmacists = new ObservableCollection<Pharmacist>();
        private ObservableCollection<string> pharmacistsName = new ObservableCollection<string>();
        private ObservableCollection<Categorie> categories = new ObservableCollection<Categorie>();
        private ObservableCollection<string> categoriesName = new ObservableCollection<string>();
        private ObservableCollection<Dose> doses = new ObservableCollection<Dose>();
        private ObservableCollection<string> dosesList = new ObservableCollection<string>();
        private ObservableCollection<string> selectedDosesList = new ObservableCollection<string>();
        private ObservableCollection<Recipe> recipes = new ObservableCollection<Recipe>();
        private ObservableCollection<RecipeView> recipesViewList = new ObservableCollection<RecipeView>();
        private List<RecipeView> selectedRecipes = new List<RecipeView>();
        private ObservableCollection<string> recipePivotCounter = new ObservableCollection<string>();
        private Patient selected_patient;

        public MainPage()
        {
            this.InitializeComponent();
            fetchData();
        }

        private void fetchData()
        {
            
            doctorsList = DoctorAL.getDoctors();
            doctorName = DoctorAL.getDoctorsName();
            cboxDokter.ItemsSource = doctorName;
            cboxResepDokter.ItemsSource = doctorName;

            patients = PatientAL.getPatients();
            patientsName = PatientAL.getPatientsName();
            cboxPasien.ItemsSource = patientsName;
            cboxResepPasien.ItemsSource = patientsName;

            pharmacists = PharmacistsAL.getPharmacists();
            pharmacistsName = PharmacistsAL.getPharmacistsName();
            cboxApoteker.ItemsSource = pharmacistsName;

            medicinesList = MedicineAL.getMedicines();
            medicinesName = MedicineAL.getMedicinesName();
            cboxObat.ItemsSource = medicinesName;

            lstObat.ItemsSource = medicinesList;

            categories = CategorieAL.getCategories();
            categoriesName = CategorieAL.getCategoriesName();

            doses = DoseAL.getDoses();
            dosesList = DoseAL.getDosesList();

            refreshRecipePage();

        }

        private void refreshRecipePage()
        {
            recipesViewList = new ObservableCollection<RecipeView>();
            recipesViewList = RecipeAL.getRecipesView(doctorsList, patients, medicinesList);

            if (cboxResepPasien.SelectedItem != null || cboxResepDokter.SelectedItem != null)
            {
                string selectedPatientName = cboxResepPasien.SelectedItem.ToString();
                string selectedDoctorName = cboxResepDokter.SelectedItem.ToString();
                DateTime selectedDate = tglResep_resepPivot.Date.Date;

                var var_selectedRecipes = recipesViewList.Where(x => x.patientName == selectedPatientName &&
                                            x.doctorName == selectedDoctorName && x.recipeDate == selectedDate);
                selectedRecipes = var_selectedRecipes.ToList();
                lstResepObat.ItemsSource = selectedRecipes;
                lstResepDosis.ItemsSource = selectedRecipes;
                lstHarga.ItemsSource = selectedRecipes;
                lstKeterangan.ItemsSource = selectedRecipes;
            }
        }

        private void cboxResepPasien_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void btnCariResep_Click(object sender, RoutedEventArgs e)
        {
            if (cboxResepPasien.SelectedItem != null || cboxResepDokter.SelectedItem != null)
            {
                string selectedPatientName = cboxResepPasien.SelectedItem.ToString();
                string selectedDoctorName = cboxResepDokter.SelectedItem.ToString();
                DateTime selectedDate = tglResep_resepPivot.Date.Date;

                var var_selectedRecipes = recipesViewList.Where(x => x.patientName == selectedPatientName &&
                                            x.doctorName == selectedDoctorName && x.recipeDate == selectedDate);

                int totalPrice = 0;
                int counter = 0;
                if(selectedRecipes.Count > 0)
                {
                    foreach (RecipeView r in selectedRecipes)
                    {
                        counter++;
                        recipePivotCounter.Add(counter.ToString());
                        totalPrice += r.total_price;
                    }

                }
                selectedRecipes = var_selectedRecipes.ToList();
                //lstNo.ItemsSource = recipePivotCounter;
                lstResepObat.ItemsSource = selectedRecipes;
                lstResepDosis.ItemsSource = selectedRecipes;
                lstHarga.ItemsSource = selectedRecipes;
                lstKeterangan.ItemsSource = selectedRecipes;
                txtTotalHarga.Text = totalPrice.ToString();

            }
        }

        private static async Task<string> req_adapter(string urlAddress)
        {

            try
            {
                HttpClient client = new HttpClient();
                HttpResponseMessage response = await client.GetAsync(urlAddress);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();             
                return responseBody;
            }
            catch
            {
                return "{\"success\" : false}";
            }
            //client.Dispose(true);
        }

        private async void btnGanti_Click(object sender, RoutedEventArgs e)
        {
            Connect conn = new Connect();
            string result = await conn.req_adapter("http://localhost:8080/e_recipe_API/e-recipe-api/public/v1/medicines/view-all");
            dynamic obj = JsonConvert.DeserializeObject(result);
            if (obj.success == "true")
            {
                string name = obj.data[0].name;
                txtKetBaru.Text = name;
            }
            else
            {
                string message = obj.success;
                txtKetBaru.Text = message;
            }
        }

        private void cboxDokter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var comboBoxItem = cboxDokter.Items[cboxDokter.SelectedIndex] as ComboBoxItem;
            if (comboBoxItem != null)
            {
                string selectedcmb = comboBoxItem.Content.ToString();
                txtKetBaru.Text = selectedcmb;
            }
            
        }

        private void cboxPasien_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cb = sender as ComboBox;
            string selected = cb.SelectedItem.ToString();
            selected_patient = patients.Single(x => x.name == selected);
            int age = selected_patient.age;
            txtUmur.Text = age.ToString();
        }

        private void cboxObat_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cb = sender as ComboBox;
            string selected = cb.SelectedItem.ToString();
            Medicine medicine = medicinesList.Single(x => x.name == selected);
            int categorie_id = medicine.category_id;
            txtDosis.Text = categorie_id.ToString();
            Categorie categorie = categories.Single(x => x.id == categorie_id);
            
            foreach (Dose d in doses)
            {
                if(d.category_id == categorie.id && selected_patient.age >= d.min_age && selected_patient.age <= d.max_age)
                {
                    txtDosis.Text = d.doseString.ToString();
                    break;
                }
            }
           // txtDosis.Text = categorie.name;


        }

        private void btnTambah_Click(object sender, RoutedEventArgs e)
        {
            //int no_resep = Int32.Parse(txtNoResep.Text);
            string apoteker = cboxApoteker.SelectedItem.ToString();
            string dokter = cboxDokter.SelectedItem.ToString();
            string pasien = cboxPasien.SelectedItem.ToString();
            string umur = txtUmur.Text.ToString();
            // string kode_obat = txtKodeObat.Text.ToString();
            string obat = cboxObat.SelectedItem.ToString();
            string dosis = txtDosis.Text.ToString();
            // string jmlKurang = txtJumlahKurang.Text.ToString();
            // string keterangan = txtKetBaru.Text.ToString();

            Pharmacist apotekerOBJ = pharmacists.Single(x => x.name == apoteker);
            int apotekerID = apotekerOBJ.id;

            Doctor dokterOBJ = doctorsList.Single(x => x.name == dokter);
            int dokterID = dokterOBJ.id;

            Patient pasienOBJ = patients.Single(x => x.name == pasien);
            int pasienID = pasienOBJ.id;

            Medicine obatOBJ = medicinesList.Single(x => x.name == obat);
            int obatID = obatOBJ.id;

            Recipe recipe = new Recipe(1, 1, dosis, dokter, 1, obatID,
                dokterID, pasienID, 1);
            RecipeAL.createRecipe(recipe);

            txtDosis.Text = "";
            refreshRecipePage();

            //txtKetBaru.Text = RecipeAL.debugRecipes();
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            refreshRecipePage();
        }
    }
}
