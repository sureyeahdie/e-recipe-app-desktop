﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    public class Medicine
    {
        public int id { get; private set; }
        public int price { get; private set; }
        public int category_id { get; private set; }
        public int amount { get; private set; }
        public int unit { get; private set; }
        public string name { get; private set; }

        public Medicine(int id, string name, int price, int category_id, int amount, int unit)
        {
            this.id = id;
            this.name = name;
            this.price = price;
            this.category_id = category_id;
            this.amount = amount;
            this.unit = unit;
        }

      
    }
}
