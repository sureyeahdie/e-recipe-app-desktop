﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    public class Patient
    {
        public int id { get; private set; }
        public string name { get; private set; }
        public string email { get; private set; }
        public string phone_number { get; private set; }
        public string address { get; private set; }
        public DateTime birth_date { get; private set; }
        public int year { get; private set; }
        public int month { get; private set; }
        public int day { get; private set; }
        public string gender { get; private set; }

        public int age { get; private set; }

        public Patient(int id, string name, string email, string phone_number, 
            string address, int year, int month, int day, string gender)
        {
            this.id = id;
            this.name = name;
            this.email = email;
            this.phone_number = phone_number;
            this.birth_date = new DateTime(year, month, day);
            this.gender = gender;
            DateTime localTime = DateTime.Now;
            this.age = localTime.Year - birth_date.Year;
            
        }
    }
}
