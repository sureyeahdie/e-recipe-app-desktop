﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    public class MedicineAL : INotifyPropertyChanged
    {
        private static ObservableCollection<Medicine> medicines = new ObservableCollection<Medicine>();
        private static ObservableCollection<string> medicinesName = new ObservableCollection<string>();

        private static string data;
        public event PropertyChangedEventHandler PropertyChanged;
            
        private static async void setMedicinesList()
        {
            Connect conn = new Connect();
            medicines = new ObservableCollection<Medicine>();
            string result = await conn.req_adapter("medicines/view-all");
            data = result;
            dynamic obj = JsonConvert.DeserializeObject(result);
            if (obj.success == "true")
            {
                foreach (var m in obj.data)
                {
                    string str_id = m.id;
                    string name = m.name;
                    string str_price = m.price;
                    string str_category_id = m.category_id;
                    string str_amount = m.amount;
                    string str_unit = m.unit;
                    
                    int id = Int32.Parse(str_id);
                    int price = Int32.Parse(str_price);
                    int category_id = Int32.Parse(str_category_id);
                    int amount = Int32.Parse(str_amount);
                    int unit = Int32.Parse(str_unit);
                    Medicine med = new Medicine(id, name, price, category_id, amount, unit);
                    medicines.Add(med);
                }
                setMedicinesName();
            }
            else
            {
                var msg = new Windows.UI.Popups.MessageDialog(
                    "No Data Found.");
            }

        }

        private static void setMedicinesName()
        {
            foreach (Medicine d in medicines)
            {
                data = d.name;
                medicinesName.Add(d.name.ToString());
            }

        }

        public static ObservableCollection<Medicine> getMedicines()
        {
            setMedicinesList();
            return medicines;
        }

        public static ObservableCollection<string> getMedicinesName()
        {

            return medicinesName;
        }

        public static string debugMedicines()
        {
            return data;
        }

    }
}
